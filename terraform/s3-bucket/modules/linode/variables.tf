variable "project_slug" {
    type = string
    description = "Project name"
}

variable "bucket_name" {
    type = string
    description = "Bucket name"
}

variable "bucket_label" {
    type = string
    description = "Bucket label"
}

variable "environment" {
    type = string
    description = "Environment, e.g. main for production, stage, a branch name etc."
    default = "main"
}

variable "cluster" {
    type = string
    description = "Linode cluster to create bucket, eu-central-1, us-east-1, us-southeast-1 or ap-south-1"
    default = "eu-central-1"
}

variable "expiration_active" {
    type = bool
    description = "Contents of bucket gets auto deleted after `experation_date`, default: true"
    default = true
}

variable "expiration_days" {
    type = string
    description = "Bucket content expiriation in days after object creation, default: 7 days"
    default = 7
}

variable "acl" {
    type = string
    description = "Pre canned Linode ACL, one of `private`, `public-read`, `authenticated-read`, `public-read-write` or `custom`"
    default = "private"
}