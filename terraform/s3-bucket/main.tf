terraform {
  required_providers {
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "3.20.0"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.16.1"
    }
  }
}

data "gitlab_project" "project" {
  id = var.gitlab_project
}

locals {
    bucket_name = var.environment == "main" ? data.gitlab_project.project.path : "${var.environment}.${data.gitlab_project.project.path}"
    bucket_prefix = "${local.bucket_name}.${var.subzone}"
    bucket_label = "${local.bucket_prefix}.${var.zone}"
    bucket_uri = "s3://${local.bucket_label}"
}

module "s3_bucket" {
    source = "./modules/linode"

    project_slug = data.gitlab_project.project.path
    bucket_name = local.bucket_name
    bucket_label = local.bucket_label
    environment = var.environment
    cluster = var.cluster
    expiration_active = var.expiration_active
    expiration_days = var.expiration_days
    acl = var.acl
}

data "cloudflare_zone" "zone" {
  name = var.zone
}

resource "cloudflare_record" "bucket_domain" {
  zone_id = data.cloudflare_zone.zone.zone_id
  name    = local.bucket_prefix
  value   = "${local.bucket_label}.${var.cluster}.linodeobjects.com"
  type    = "CNAME"
  ttl     = 1
  proxied = true
}

resource "gitlab_project_variable" "aws_access_id" {
  project = data.gitlab_project.project.id
  key     = "AWS_ACCESS_KEY_ID"
  protected = var.environment == "main" ? true : false
  masked = false
  value   = module.s3_bucket.aws_access_key_id
  environment_scope = var.environment
}

resource "gitlab_project_variable" "aws_access_secret_key" {
  project = data.gitlab_project.project.id
  key     = "AWS_SECRET_ACCESS_KEY"
  protected = var.environment == "main" ? true : false
  masked = true
  value   = module.s3_bucket.aws_secret_access_key
  environment_scope = var.environment
}

resource "gitlab_project_variable" "tmp_bucket" {
  count = var.environment == "main" ? 0 : 1
  project = data.gitlab_project.project.id
  key     = "S3_TMP_BUCKET"
  protected = false
  masked = false
  value   = local.bucket_uri
  environment_scope = var.environment
}

resource "gitlab_project_variable" "read_aws_access_id" {
  count = var.environment == "main" ? 1 : 0
  project = data.gitlab_project.project.id
  key     = "READ_AWS_ACCESS_KEY_ID"
  protected = false
  masked = false
  value   = module.s3_bucket.read_aws_access_key_id
  environment_scope = "*"
}

resource "gitlab_project_variable" "read_aws_access_secret_key" {
  count = var.environment == "main" ? 1 : 0
  project = data.gitlab_project.project.id
  key     = "READ_AWS_SECRET_ACCESS_KEY"
  protected = false
  masked = true
  value   = module.s3_bucket.read_aws_secret_access_key
  environment_scope = "*"
}

resource "gitlab_project_variable" "project_bucket" {
  count = var.environment == "main" ? 1 : 0
  project = data.gitlab_project.project.id
  key     = "S3_PROJECT_BUCKET"
  protected = false
  masked = false
  value   = local.bucket_uri
  environment_scope = "*"
}

resource "gitlab_project_variable" "s3_endpoint" {
  count = var.environment == "main" ? 1 : 0
  project = data.gitlab_project.project.id
  key     = "S3_ENDPOINT"
  protected = false
  masked = false
  value   = module.s3_bucket.s3_endpoint
  environment_scope = "*"
}
