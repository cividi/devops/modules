variable "gitlab_project" {
    type = string
    description = "GitLab project path including groups path, e.g. foo/bar/baz"
}

variable "zone" {
    type = string
    description = "tld for bucket custom domain, default: cividi.io"
    default = "cividi.io"
}

variable "environment" {
    type = string
    description = "Environment, e.g. main for production, stage, a branch name etc."
    default = "main"
}

variable "cluster" {
    type = string
    description = "Linode cluster to create bucket, eu-central-1, us-east-1, us-southeast-1 or ap-south-1"
    default = "eu-central-1"
}

variable "subzone" {
    type = string
    description = "Subzone for TLD, default s3"
    default = "s3"
}

variable "expiration_active" {
    type = bool
    description = "Contents of bucket gets auto deleted after `experation_date`, default: true"
    default = true
}

variable "expiration_days" {
    type = string
    description = "Bucket content expiriation in days after object creation, default: 7 days"
    default = 7
}

variable "acl" {
    type = string
    description = "Pre canned Linode ACL, one of `private`, `public-read`, `authenticated-read`, `public-read-write` or `custom`"
    default = "private"
}