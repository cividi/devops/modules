# Cividi Modules

Includes:
- [Terraform modules](terraform/) - used in projects, platform, etc.
- [frictionless plugins](frictionless/)
- [Python modules](python)
- [Docker images](docker/) - custom(ized) docker images
- [node](node/)

This repo is intended to incubate small useful modules and auto-publish new version to the project registry for consumption in projects and products.

Modules that mature beyond the useable stage can be spun out as seperate repos.
