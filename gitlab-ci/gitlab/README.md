# Automatic project releases

To add automatic release tags and changelogs
- copy `package.json`, `.release-it.json` to your project root
- add a node exceptions to your `.gitignore`
- add `SSH_PRIVATE_KEY` and `GITLAB_TOKEN` to the CI variables
- add the following sections to your `.gitlab-ci.yml`

```yaml
include:
  - project: 'cividi/devops/modules'
    ref: main
    file: /gitlab-ci/release/release.base.gitlab-ci.yml

stages:
  - release

release:pre:
  extends: .release
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

release:
  extends: .release
  script:
    - npx release-it --ci
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

```

Now every push to a work branch triggers a beta release based on the commit message and a merge onto the main branch a full release. Whether the recommended version bump is major, minor or a patch depends on the commit messages used prior to the release. This is based on the [angular convention](https://github.com/angular/angular/blob/22b96b9/CONTRIBUTING.md#-commit-message-guidelines) and the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) standard.

The basics:
- Every commit starts with a type and optional scope and a colon `type(scope): <commit message>`, e.g. `feat(ci): automatic releases`, `feat` being the type and `ci` the scope/module of the change
- If either a `!` is included before the colon or the commit body includes `BREAKING CHANGE: <description>` the major version is automatically increased
- the following types are recommended to use:
    * **feat**: A new feature / output / pipeline
    * **fix**: A bug fix
    * **build**: Changes that affect the build system or external dependencies (example scopes: requirements)
    * **ci**: Changes to the CI configuration files and scripts (example scopes: gitlab-ci)
    * **docs**: Documentation only changes
    * **perf**: A code change that improves performance
    * **refactor**: A code change that neither fixes a bug nor adds a feature
    * **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
    * **test**: Adding missing tests or correcting existing tests