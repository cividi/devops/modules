terraform {
  required_providers {
    linode = {
      source = "linode/linode"
      version = "1.28.1"
    }
    random = {
      source = "hashicorp/random"
      version = "3.3.2"
    }
  }
}

locals {
  expiration_active = var.environment == "main" ? false : var.expiration_active
  expiration_days = var.expiration_days
}

resource "random_id" "access_token" {
  keepers = {
    env = var.environment
  }
  prefix = "${var.project_slug}-"
  byte_length = 8
}

resource "linode_object_storage_key" "access" {
  label = random_id.access_token.b64_url
  # bucket_access {
  #   bucket_name = var.bucket_label
  #   cluster = var.cluster
  #   permissions = "read_write"
  # }
}

resource "linode_object_storage_bucket" "bucket" {
  access_key = linode_object_storage_key.access.access_key
  secret_key = linode_object_storage_key.access.secret_key

  acl = var.acl

  cluster = var.cluster
  label   = var.bucket_label
  versioning = true

  lifecycle_rule {
    id      = "${var.bucket_name}-${var.environment}-acl-rule"
    enabled = local.expiration_active

    abort_incomplete_multipart_upload_days = 5

    expiration {
      days = local.expiration_days
    }
  }
}

resource "linode_object_storage_object" "in_dir" {
    bucket  = linode_object_storage_bucket.bucket.label
    cluster = var.cluster
    key     = "in/readme.md"

    access_key = linode_object_storage_key.access.access_key
    secret_key = linode_object_storage_key.access.secret_key

    content          = "# data in\nProject folder for incoming data. Only modify main project bucket in folder."
    content_type     = "text/plain"
    content_language = "en"
}

resource "linode_object_storage_object" "out_dir" {
    bucket  = linode_object_storage_bucket.bucket.label
    cluster = var.cluster
    key     = "out/readme.md"

    access_key = linode_object_storage_key.access.access_key
    secret_key = linode_object_storage_key.access.secret_key

    content          = "# data out\nProject folder for outgoing data."
    content_type     = "text/plain"
    content_language = "en"
}

resource "random_id" "access_read_token" {
  keepers = {
    env = var.environment
  }
  prefix = "${var.project_slug}-read-"
  byte_length = 8
}

resource "linode_object_storage_key" "read_access" {
  count = var.environment == "main" ? 1 : 0
  label = random_id.access_read_token.b64_url
  bucket_access {
    bucket_name = linode_object_storage_bucket.bucket.label
    cluster = var.cluster
    permissions = "read_only"
  }
}