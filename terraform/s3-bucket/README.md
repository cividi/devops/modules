# S3 Bucket

Sets up a S3 project bucket tied to an environment.

```mermaid
flowchart TD;
    LINODE_TOKEN --> bucket_token;
    bucket_token --> AWS_ACCESS_ID;
    bucket_token --> AWS_SECRET_KEY;
    AWS_ACCESS_ID --> REPO_VARS;
    AWS_SECRET_KEY --> REPO_VARS;
    linode_cluster --> endpoint_url;
    endpoint_url --> outputs;
    GITLAB_TOKEN --> PROJECT_ID;
    PROJECT_ID --> pn;
    REPO_VARS_MANUAL -.-> zone;
    REPO_VARS_MANUAL -.-> linode_cluster;
    REPO_VARS_MANUAL -.-> subzone;
    REPO_VARS_MANUAL -.- REPO_VARS;
    REPO_PATH --> PROJECT_ID;
    subzone[subzone, e.g. 's3.'] --> bucket_prefix;
    pn[project_path] --> bucket_prefix;
    zone[zone, e.g. 'cividi.io'] --> bucket_label[bucket_label, e.g. 's3.example.cividi.io'];
    bucket_prefix --> bucket_label;
    bucket_label --> bucket_url;
    bucket_label --> bucket_uri;
    bucket_uri --> outputs;
    bucket_url --> outputs;
```

## Environment Variables

| name | default | description |
| ---  | ---     | --- |
| `LINODE_TOKEN` | None | API `api token` for Linode account |
| `GITLAB_TOKEN` | None | API `api token` for GitLab account/group |
| `CLOUDFLARE_TOKEN` | None | API `api token` for Cloudflare account |
| `TF_VAR_zone` | `cividi.io` | Domain zone to use for custom bucket domains |
| `TF_VAR_environment` | `main` | Terraform state backend / environment |
| `TF_VAR_cluster` | `eu-central-1` | Linode cluster |
| `TF_VAR_subzone` | `s3` | Subdomain prefix |
| `TF_VAR_expiration_active` | `true` | Auto expire content in bucket |
| `TF_VAR_expiration_days` | `7` | Auto expire content after number of days |
| `TF_VAR_acl` | `private` | Bucket access control, can be private, public-read, authenticated-read, public-read-write or custom |
