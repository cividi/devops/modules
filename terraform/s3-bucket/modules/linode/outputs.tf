output "aws_access_key_id" {
  value = linode_object_storage_key.access.access_key
}

output "aws_secret_access_key" {
  value = linode_object_storage_key.access.secret_key
}

output "read_aws_access_key_id" {
  value = var.environment == "main" ? linode_object_storage_key.read_access[0].access_key : ""
}

output "read_aws_secret_access_key" {
  value = var.environment == "main" ? linode_object_storage_key.read_access[0].secret_key : ""
}

output "s3_endpoint" {
  value = "https://${var.cluster}.linodeobjects.com"
}
